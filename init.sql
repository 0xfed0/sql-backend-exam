-- create a table
CREATE TABLE clients(
    category TEXT NOT NULL,
    firstname TEXT NOT NULL,
    lastname TEXT NOT NULL,
    email TEXT NOT NULL,
    gender TEXT NOT NULL,
    birthDate DATE NOT NULL
);

COPY clients FROM '/docker-entrypoint-initdb.d/dataset.txt' DELIMITERS ',' CSV HEADER;