import os
from flask import Flask, Response
import psycopg2

app = Flask(__name__)
db = psycopg2.connect(database=os.environ["POSTGRES_DB"],
                        host="database",
                        user=os.environ["POSTGRES_USER"],
                        password=os.environ["POSTGRES_PASSWORD"],
                        port="5432")
cursor = db.cursor()



@app.route('/')
def health():
	return "Ok"

@app.route('/clients')
def clients():
    cursor.execute("SELECT * FROM clients")
    csv = "\n".join([','.join([str(v) for v in values]) for values in cursor.fetchall()])
    return Response(
            csv,
            mimetype="text/csv",
            headers={"Content-disposition":
                    "attachment; filename=clients.csv"})

if __name__ == '__main__':
	app.run(host='0.0.0.0', port=8000)